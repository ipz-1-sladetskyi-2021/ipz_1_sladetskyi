﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Timer = System.Windows.Forms.Timer;

namespace lab_ipz
{
    static class Program
    {
        static string log = "@";
        static string reg = "#";
        static string my_q = "*";
        public static string user_data;
        public static System.Windows.Forms.Timer holder = new Timer { Interval = 1000 };
        //static String server = "192.168.1.4";//wi-fi
       // static String server = "3.134.39.220";//ngrok

        static String server = "127.0.0.1";//sql
        static Int32 port = 3306;
        // static Int32 port = 13000;// ngrok

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new main_form());
 
        }
        public static string get_reg()
        {
            return reg;
        }
        public static string get_my_q()
        {
            return my_q;
        }

        public static string get_actual_id( String message,string id)
        {
            try
            {
                TcpClient client = new TcpClient();
                client.Connect(server, port);
                NetworkStream stream = client.GetStream();
                    // Перетворення повідомлення в  UTF8.
                    Byte[] data = System.Text.Encoding.UTF8.GetBytes(message + id);
                    // Відправлення повідомлення на сервер. 
                    stream.Write(data, 0, data.Length);
                    // Масив байтів для отримання повідомлення від сервера.
                    data = new Byte[256];
                    String response = String.Empty;
                    // Читання отриманого повідомлення від сервера.
                    Int32 bytes = stream.Read(data, 0, data.Length);
                    response = System.Text.Encoding.UTF8.GetString(data, 0, bytes);
                    data = System.Text.Encoding.UTF8.GetBytes("exit");
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                    client.Close();
                    return response;
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
                holder.Enabled = false;
                return "error";
            }
        }
        public static void insert_message( String message, String command)
        {
            try
            {
                TcpClient client = new TcpClient(server, port);
                NetworkStream stream = client.GetStream();
                    // Перетворення повідомлення в  UTF8.
                    Byte[] data = System.Text.Encoding.UTF8.GetBytes(message);
                    // Відправлення повідомлення на сервер. 
                    stream.Write(data, 0, data.Length);
                    // Масив байтів для отримання повідомлення від сервера.
                    data = new Byte[256];
                    String response = String.Empty;
                    // Читання отриманого повідомлення від сервера.
                     Int32 bytes = stream.Read(data, 0, data.Length);
                    response = System.Text.Encoding.UTF8.GetString(data, 0, bytes);
                   if(message == response)
                    {
                        data = System.Text.Encoding.UTF8.GetBytes(command + "exit");
                        stream.Write(data, 0, data.Length);
                    }
                stream.Close();
                client.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                holder.Enabled = false;
            }
        }

        public static string check_exist(String message)
        {
            try
            {
                TcpClient client = new TcpClient();
                IPAddress ipAddress = IPAddress.Parse(server);
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress,port);

                client.Connect(ipEndPoint);
                NetworkStream stream = client.GetStream();
                // Перетворення повідомлення в  UTF8.
                Byte[] data = System.Text.Encoding.UTF8.GetBytes(message);
                // Відправлення повідомлення на сервер. 
                stream.Write(data, 0, data.Length);
                // Масив байтів для отримання повідомлення від сервера.
                data = new Byte[256];
                String response = String.Empty;
                // Читання отриманого повідомлення від сервера.
                Int32 bytes = stream.Read(data, 0, data.Length);
                response = System.Text.Encoding.UTF8.GetString(data, 0, bytes);
                if (message + "yes" == response)
                {
                    data = System.Text.Encoding.UTF8.GetBytes(message[0].ToString() + "exit");
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                    client.Close();
                    return "true";
                }
                else
                {
                    data = System.Text.Encoding.UTF8.GetBytes(message[0].ToString() + "exit");
                    stream.Write(data, 0, data.Length);
                    stream.Close();
                    client.Close();
                    return "false";
                }
            }
            catch (SocketException e)
            {
                //MessageBox.Show(e.Message);
                holder.Enabled = false;
                return "error";
            }
        }

       

    }
}
