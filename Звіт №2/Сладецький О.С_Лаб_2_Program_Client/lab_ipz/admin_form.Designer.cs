﻿namespace lab_ipz
{
    partial class admin_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        // <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin_form));
            this.label_admin_queue = new System.Windows.Forms.Label();
            this.button_admin_delete = new System.Windows.Forms.Button();
            this.label_people_admin = new System.Windows.Forms.Label();
            this.button_admin_back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_admin_queue
            // 
            this.label_admin_queue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_admin_queue.AutoSize = true;
            this.label_admin_queue.BackColor = System.Drawing.Color.Transparent;
            this.label_admin_queue.Font = new System.Drawing.Font("Comic Sans MS", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_admin_queue.ForeColor = System.Drawing.Color.LimeGreen;
            this.label_admin_queue.Location = new System.Drawing.Point(134, 94);
            this.label_admin_queue.Name = "label_admin_queue";
            this.label_admin_queue.Size = new System.Drawing.Size(596, 67);
            this.label_admin_queue.TabIndex = 0;
            this.label_admin_queue.Text = "Кількість людей в черзі";
            // 
            // button_admin_delete
            // 
            this.button_admin_delete.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_admin_delete.Font = new System.Drawing.Font("Comic Sans MS", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_admin_delete.Location = new System.Drawing.Point(327, 264);
            this.button_admin_delete.Name = "button_admin_delete";
            this.button_admin_delete.Size = new System.Drawing.Size(210, 55);
            this.button_admin_delete.TabIndex = 1;
            this.button_admin_delete.Text = "Наступний";
            this.button_admin_delete.UseVisualStyleBackColor = true;
            // 
            // label_people_admin
            // 
            this.label_people_admin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_people_admin.AutoSize = true;
            this.label_people_admin.BackColor = System.Drawing.Color.Transparent;
            this.label_people_admin.Font = new System.Drawing.Font("Comic Sans MS", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_people_admin.ForeColor = System.Drawing.Color.Gold;
            this.label_people_admin.Location = new System.Drawing.Point(403, 173);
            this.label_people_admin.Name = "label_people_admin";
            this.label_people_admin.Size = new System.Drawing.Size(51, 60);
            this.label_people_admin.TabIndex = 2;
            this.label_people_admin.Text = "0";
            // 
            // button_admin_back
            // 
            this.button_admin_back.Font = new System.Drawing.Font("Comic Sans MS", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_admin_back.Location = new System.Drawing.Point(376, 334);
            this.button_admin_back.Name = "button_admin_back";
            this.button_admin_back.Size = new System.Drawing.Size(133, 49);
            this.button_admin_back.TabIndex = 3;
            this.button_admin_back.Text = "Назад";
            this.button_admin_back.UseVisualStyleBackColor = true;
            // 
            // admin_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::lab_ipz.Properties.Resources.queue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(863, 492);
            this.Controls.Add(this.button_admin_back);
            this.Controls.Add(this.label_people_admin);
            this.Controls.Add(this.button_admin_delete);
            this.Controls.Add(this.label_admin_queue);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(852, 480);
            this.Name = "admin_form";
            this.Text = "Адміністратор";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_admin_queue;
        private System.Windows.Forms.Button button_admin_delete;
        private System.Windows.Forms.Label label_people_admin;
        private System.Windows.Forms.Button button_admin_back;
    }
}