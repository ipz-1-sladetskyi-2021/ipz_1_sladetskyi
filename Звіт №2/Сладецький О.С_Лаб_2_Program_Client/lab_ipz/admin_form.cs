﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_ipz
{
    public partial class admin_form : Form
    {
        public admin_form()
        {
            InitializeComponent();
            button_admin_back.Click += button_admin_back_click;
            if(Program.get_actual_id( Program.user_data, "all_id") == "error")
            {
                MessageBox.Show("Сталася помилка під час з'єднання!");
                Environment.Exit(-1);
            }
            label_people_admin.Text = Program.get_actual_id( Program.user_data, "all_id");
            Program.holder = new Timer { Interval = 1000 };
            Program.holder.Tick += HolderTick;
            Program.holder.Enabled = true;
            button_admin_delete.Click += button_admin_delete_click;
        }

        private void button_admin_delete_click(object sender, EventArgs e)
        {
            if(label_people_admin.Text == "0")
            {
                MessageBox.Show("Користувачів немає у черзі!");
                return;
            }
            else
            {
                Program.insert_message("delete_last", "");
            }
        }
        private void HolderTick(object sender, EventArgs e)
        {
            if (Program.get_actual_id( Program.user_data, "all_id") == "error")
            {
                MessageBox.Show("Сталася помилка під час з'єднання!");
                Program.holder.Enabled = false;
                return;
            }
            label_people_admin.Text = Program.get_actual_id( Program.user_data, "all_id");
            Program.holder.Enabled = true;
        }

        private void button_admin_back_click(object sender, EventArgs e)
        {
            this.Hide();
            main_form f2 = new main_form();
            f2.StartPosition = FormStartPosition.Manual;
            f2.Location = this.Location;
            f2.Size = this.Size;
            f2.ShowDialog();
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }
    }
}
