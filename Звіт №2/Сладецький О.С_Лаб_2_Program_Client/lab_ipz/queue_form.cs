﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_ipz
{
    public partial class queue_form : Form
    {
        private string check;
        private int cnt=0;
        public queue_form()
        {
            InitializeComponent();
            back_but_q.Click += back_but_q_click;
            button_queue_in.Click += button_queue_in_click;
            button_queue_out.Click += button_queue_out_click;
            check = Program.check_exist( Program.user_data + "$check");
            if (check == "false")
            {
                label_actual_id.Text = "Ви не у черзі";
            }
            else if (check == "true")
            {
                label_actual_id.Text = Program.get_actual_id( Program.user_data,"actual_id");
            }
            else
            {
                Environment.Exit(-1);
            }
            label_queue.Text = Program.get_actual_id( Program.user_data, "all_id");
            Program.holder = new Timer { Interval = 1000 };
            Program.holder.Tick += HolderTick;
            Program.holder.Enabled = true;
        }

        private void HolderTick(object sender, EventArgs e)
        {
            check = Program.check_exist( Program.user_data + "$check");
            if (check == "false")
            {
                label_actual_id.Text = "Ви не у черзі";
            }
            else if (check == "true")
            {
                label_actual_id.Text = Program.get_actual_id( Program.user_data, "actual_id");
            }
            else
            {
                Environment.Exit(-1);
            }
            label_queue.Text = Program.get_actual_id( Program.user_data, "all_id");
            if (label_actual_id.Text == "1")
            {
                label_wait.Text = "Ваша черга!";
                if (cnt == 0)
                {
                    SoundPlayer player = new SoundPlayer(lab_ipz.Properties.Resources.hehe);
                    player.Play();
                    cnt++;
                }
            }
            else if (check == "false")
            {
                label_wait.Text = "Черга чекає на вас!";
                cnt = 0;
            }
            else if (check == "true")
            {
                label_wait.Text = "Будь ласка, зачекайте...";
                cnt = 0;
            }
            else
            {
                MessageBox.Show("Сталася помилка під час з'єднання!");
                Program.holder.Enabled = false;
                return;
            }
            Program.holder.Enabled = true;
        }

        private void button_queue_out_click(object sender, EventArgs e)
        {
            check = Program.check_exist( Program.user_data + "$check");
            if (check == "true")
            {
                Program.insert_message( "%" + Program.user_data, "%");
            }
            else if (check == "false")
            {
                MessageBox.Show("Ви не у черзі");
            }
            else
            {
                Environment.Exit(-1);
            }
        }
        private void button_queue_in_click(object sender, EventArgs e)
        {
            check = Program.check_exist( "*" + Program.user_data);
            if (check == "true")
            {
                MessageBox.Show("Ви уже у черзі");
            }
            else if(check == "error")
            {
                Environment.Exit(-1);
            }
        }
        private void back_but_q_click(object sender, EventArgs e)
        {
            this.Hide();
            main_form f2 = new main_form();
            f2.StartPosition = FormStartPosition.Manual;
            f2.Location = this.Location;
            f2.Size = this.Size;
            f2.ShowDialog();
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void back_but_q_Click_1(object sender, EventArgs e)
        {

        }
    }
}
