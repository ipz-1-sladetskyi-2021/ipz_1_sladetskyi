﻿namespace lab_ipz
{
    partial class queue_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        // <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(queue_form));
            this.label1 = new System.Windows.Forms.Label();
            this.label_queue = new System.Windows.Forms.Label();
            this.button_queue_in = new System.Windows.Forms.Button();
            this.button_queue_out = new System.Windows.Forms.Button();
            this.back_but_q = new System.Windows.Forms.Button();
            this.label_text_a_id = new System.Windows.Forms.Label();
            this.label_actual_id = new System.Windows.Forms.Label();
            this.label_wait = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.LimeGreen;
            this.label1.Location = new System.Drawing.Point(180, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Людей у черзі";
            // 
            // label_queue
            // 
            this.label_queue.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_queue.BackColor = System.Drawing.Color.Transparent;
            this.label_queue.Font = new System.Drawing.Font("Comic Sans MS", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_queue.ForeColor = System.Drawing.Color.Magenta;
            this.label_queue.Location = new System.Drawing.Point(233, 162);
            this.label_queue.Name = "label_queue";
            this.label_queue.Size = new System.Drawing.Size(116, 65);
            this.label_queue.TabIndex = 1;
            this.label_queue.Text = "...";
            this.label_queue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_queue_in
            // 
            this.button_queue_in.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_queue_in.AutoSize = true;
            this.button_queue_in.Font = new System.Drawing.Font("Comic Sans MS", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_queue_in.Location = new System.Drawing.Point(187, 315);
            this.button_queue_in.Name = "button_queue_in";
            this.button_queue_in.Size = new System.Drawing.Size(214, 45);
            this.button_queue_in.TabIndex = 2;
            this.button_queue_in.Text = "Записатись у чергу";
            this.button_queue_in.UseVisualStyleBackColor = true;
            // 
            // button_queue_out
            // 
            this.button_queue_out.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button_queue_out.Font = new System.Drawing.Font("Comic Sans MS", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_queue_out.Location = new System.Drawing.Point(522, 315);
            this.button_queue_out.Name = "button_queue_out";
            this.button_queue_out.Size = new System.Drawing.Size(173, 45);
            this.button_queue_out.TabIndex = 3;
            this.button_queue_out.Text = "Вийти з черги";
            this.button_queue_out.UseVisualStyleBackColor = true;
            // 
            // back_but_q
            // 
            this.back_but_q.Font = new System.Drawing.Font("Comic Sans MS", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.back_but_q.Location = new System.Drawing.Point(389, 357);
            this.back_but_q.Name = "back_but_q";
            this.back_but_q.Size = new System.Drawing.Size(148, 48);
            this.back_but_q.TabIndex = 4;
            this.back_but_q.Text = "Назад";
            this.back_but_q.UseVisualStyleBackColor = true;
            this.back_but_q.Click += new System.EventHandler(this.back_but_q_Click_1);
            // 
            // label_text_a_id
            // 
            this.label_text_a_id.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_text_a_id.AutoSize = true;
            this.label_text_a_id.BackColor = System.Drawing.Color.Transparent;
            this.label_text_a_id.Font = new System.Drawing.Font("Comic Sans MS", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_text_a_id.ForeColor = System.Drawing.Color.LimeGreen;
            this.label_text_a_id.Location = new System.Drawing.Point(464, 124);
            this.label_text_a_id.Name = "label_text_a_id";
            this.label_text_a_id.Size = new System.Drawing.Size(275, 39);
            this.label_text_a_id.TabIndex = 6;
            this.label_text_a_id.Text = "Ваш номер у черзі";
            // 
            // label_actual_id
            // 
            this.label_actual_id.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_actual_id.BackColor = System.Drawing.Color.Transparent;
            this.label_actual_id.Font = new System.Drawing.Font("Comic Sans MS", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_actual_id.ForeColor = System.Drawing.Color.Magenta;
            this.label_actual_id.Location = new System.Drawing.Point(471, 162);
            this.label_actual_id.Name = "label_actual_id";
            this.label_actual_id.Size = new System.Drawing.Size(246, 64);
            this.label_actual_id.TabIndex = 7;
            this.label_actual_id.Text = "...";
            this.label_actual_id.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_wait
            // 
            this.label_wait.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label_wait.AutoSize = true;
            this.label_wait.BackColor = System.Drawing.Color.Transparent;
            this.label_wait.Font = new System.Drawing.Font("Comic Sans MS", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_wait.ForeColor = System.Drawing.Color.Gold;
            this.label_wait.Location = new System.Drawing.Point(269, 239);
            this.label_wait.Name = "label_wait";
            this.label_wait.Size = new System.Drawing.Size(366, 39);
            this.label_wait.TabIndex = 8;
            this.label_wait.Text = "Будь ласка, зачекайте...";
            this.label_wait.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // queue_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::lab_ipz.Properties.Resources.queue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(900, 499);
            this.Controls.Add(this.label_wait);
            this.Controls.Add(this.label_actual_id);
            this.Controls.Add(this.label_text_a_id);
            this.Controls.Add(this.button_queue_out);
            this.Controls.Add(this.button_queue_in);
            this.Controls.Add(this.label_queue);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.back_but_q);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(852, 480);
            this.Name = "queue_form";
            this.Text = "Черга";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_queue;
        private System.Windows.Forms.Button button_queue_in;
        private System.Windows.Forms.Button button_queue_out;
        private System.Windows.Forms.Button back_but_q;
        private System.Windows.Forms.Label label_text_a_id;
        private System.Windows.Forms.Label label_actual_id;
        private System.Windows.Forms.Label label_wait;
    }
}