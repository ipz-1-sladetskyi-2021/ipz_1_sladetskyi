﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace lab_ipz
{
    public partial class main_form : Form
    {
        public main_form()
        {
            InitializeComponent();
            link_login.Click += link_log_in;
            log_in_but.Click += log_in_but_click;
            checkBox_log_pass.CheckedChanged += checkBox_log_pass_check;
        }

        private void checkBox_log_pass_check(object sender, EventArgs e)
        {
            if (checkBox_log_pass.Checked == true)
                textBox_pass.UseSystemPasswordChar = false;
            else
                textBox_pass.UseSystemPasswordChar = true;
        }
        private void log_in_but_click(object sender, EventArgs e)
        {
            if (textBox_log.Text.Length != 0 && textBox_pass.Text.Length != 0)
            {
                if (textBox_log.Text.Contains(" ") ||  textBox_pass.Text.Contains(" "))
                {
                    MessageBox.Show("Поля не можуть містити пробіли");
                }
                else
                {
                    string check = Program.check_exist( ("@" + textBox_log.Text + "$" + textBox_pass.Text));
                    if (check == "false")
                    {
                        MessageBox.Show("Такого користувача не існує");
                        textBox_log.Focus();
                        return;
                    }
                    else if (check == "true")
                    {
                        Program.user_data = textBox_log.Text;
                        if(textBox_log.Text == "admin" && textBox_pass.Text == "admin")
                        {
                            this.Hide();
                            admin_form f2 = new admin_form();
                            f2.StartPosition = FormStartPosition.Manual;
                            f2.Location = this.Location;
                            f2.Size = this.Size;
                            f2.ShowDialog();
                        }
                        else
                        {
                            this.Hide();
                            queue_form f2 = new queue_form();
                            f2.StartPosition = FormStartPosition.Manual;
                            f2.Location = this.Location;
                            f2.Size = this.Size;
                            f2.ShowDialog();
                        }
                        if (System.Windows.Forms.Application.MessageLoop)
                        {
                            // WinForms app
                            System.Windows.Forms.Application.Exit();
                        }
                        else
                        {
                            // Console app
                            System.Environment.Exit(1);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Сталася помилка під час з'єднання!");
                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("Введіть логін і пароль!");
            }
        }
            private void link_log_in(object sender, EventArgs e)
        {
                this.Hide();
                reg_form f2 = new reg_form();
                f2.StartPosition = FormStartPosition.Manual;
                f2.Location = this.Location;
                f2.Size = this.Size;
                f2.ShowDialog();
                if (System.Windows.Forms.Application.MessageLoop)
                {
                    // WinForms app
                    System.Windows.Forms.Application.Exit();
                }
                else
                {
                    // Console app
                    System.Environment.Exit(1);
                }
        }

        private void main_form_Load(object sender, EventArgs e)
        {
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }

}
