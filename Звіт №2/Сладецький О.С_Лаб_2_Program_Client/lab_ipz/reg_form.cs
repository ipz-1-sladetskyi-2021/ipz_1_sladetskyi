﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab_ipz
{
    public partial class reg_form : Form
    {
        public reg_form()
        {
            InitializeComponent();
            reg_back.Click += reg_back_click;
            reg_but.Click += reg_but_click;
        }
        private void reg_back_click(object sender, EventArgs e)
        {
            this.Hide();
            main_form f2 = new main_form();
            f2.StartPosition = FormStartPosition.Manual;
            f2.Location = this.Location;
            f2.Size = this.Size;
            f2.ShowDialog();
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void reg_but_click(object sender, EventArgs e)
        {
            if (textBox_reg_log.Text.Length != 0 && textBox_reg_pass.Text.Length != 0 && textBox_reg_rep_pass.Text.Length != 0)
            {
                if (textBox_reg_log.Text.Contains(" ") || textBox_reg_pass.Text.Contains(" ")
                    || textBox_reg_log.Text.Contains("@") || textBox_reg_pass.Text.Contains("@")
                    || textBox_reg_log.Text.Contains("#") || textBox_reg_pass.Text.Contains("#")
                    || textBox_reg_log.Text.Contains("$") || textBox_reg_pass.Text.Contains("$")
                    || textBox_reg_log.Text.Contains("*") || textBox_reg_pass.Text.Contains("*")
                    || textBox_reg_log.Text.Contains("%") || textBox_reg_pass.Text.Contains("%"))
                {
                    MessageBox.Show("Поля не можуть містити пробіли або симвои '$','@','#','*','%'");
                }
                else
                {
                    if (textBox_reg_pass.Text != textBox_reg_rep_pass.Text)
                    {
                        MessageBox.Show("Паролі не збігаються");
                        textBox_reg_rep_pass.Clear();
                        textBox_reg_rep_pass.Focus();
                        return;
                    }
                    string check = Program.check_exist( "#" + textBox_reg_log.Text);
                    if (check == "true")
                    {
                        MessageBox.Show("Цей логін занятий");
                        textBox_reg_log.Focus();
                        return;
                    }
                    else if (check == "false")
                    {
                        Program.insert_message(("#" + textBox_reg_log.Text + "$" + textBox_reg_pass.Text), Program.get_reg());
                        Program.user_data = textBox_reg_log.Text;
                        this.Hide();
                        queue_form f2 = new queue_form();
                        f2.StartPosition = FormStartPosition.Manual;
                        f2.Location = this.Location;
                        f2.Size = this.Size;
                        f2.ShowDialog();
                        if (System.Windows.Forms.Application.MessageLoop)
                        {
                            // WinForms app
                            System.Windows.Forms.Application.Exit();
                        }
                        else
                        {
                            // Console app
                            System.Environment.Exit(1);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Сталася помилка під час з'єднання!");
                        return;
                    }
                }
            }
        }
    }
}
