﻿namespace lab_ipz
{
    partial class reg_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        // <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(reg_form));
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_reg_pass = new System.Windows.Forms.TextBox();
            this.textBox_reg_log = new System.Windows.Forms.TextBox();
            this.textBox_reg_rep_pass = new System.Windows.Forms.TextBox();
            this.reg_but = new System.Windows.Forms.Button();
            this.reg_back = new System.Windows.Forms.Button();
            this.label_repeat_pass = new System.Windows.Forms.Label();
            this.label_pass_reg = new System.Windows.Forms.Label();
            this.log_reg = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Aquamarine;
            this.label1.Location = new System.Drawing.Point(350, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(340, 84);
            this.label1.TabIndex = 0;
            this.label1.Text = "Реєстрація";
            // 
            // textBox_reg_pass
            // 
            this.textBox_reg_pass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_reg_pass.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_reg_pass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox_reg_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_reg_pass.ForeColor = System.Drawing.Color.Maroon;
            this.textBox_reg_pass.Location = new System.Drawing.Point(350, 212);
            this.textBox_reg_pass.MaxLength = 24;
            this.textBox_reg_pass.Multiline = true;
            this.textBox_reg_pass.Name = "textBox_reg_pass";
            this.textBox_reg_pass.Size = new System.Drawing.Size(190, 30);
            this.textBox_reg_pass.TabIndex = 12;
            this.textBox_reg_pass.UseSystemPasswordChar = true;
            // 
            // textBox_reg_log
            // 
            this.textBox_reg_log.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_reg_log.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox_reg_log.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox_reg_log.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_reg_log.ForeColor = System.Drawing.Color.Maroon;
            this.textBox_reg_log.Location = new System.Drawing.Point(260, 127);
            this.textBox_reg_log.MaxLength = 24;
            this.textBox_reg_log.Name = "textBox_reg_log";
            this.textBox_reg_log.Size = new System.Drawing.Size(190, 30);
            this.textBox_reg_log.TabIndex = 11;
            // 
            // textBox_reg_rep_pass
            // 
            this.textBox_reg_rep_pass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox_reg_rep_pass.BackColor = System.Drawing.SystemColors.Window;
            this.textBox_reg_rep_pass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox_reg_rep_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_reg_rep_pass.ForeColor = System.Drawing.Color.Maroon;
            this.textBox_reg_rep_pass.Location = new System.Drawing.Point(485, 287);
            this.textBox_reg_rep_pass.MaxLength = 24;
            this.textBox_reg_rep_pass.Multiline = true;
            this.textBox_reg_rep_pass.Name = "textBox_reg_rep_pass";
            this.textBox_reg_rep_pass.Size = new System.Drawing.Size(190, 30);
            this.textBox_reg_rep_pass.TabIndex = 14;
            this.textBox_reg_rep_pass.UseSystemPasswordChar = true;
            // 
            // reg_but
            // 
            this.reg_but.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.reg_but.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.reg_but.Font = new System.Drawing.Font("Comic Sans MS", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.reg_but.Location = new System.Drawing.Point(34, 3);
            this.reg_but.Name = "reg_but";
            this.reg_but.Size = new System.Drawing.Size(334, 50);
            this.reg_but.TabIndex = 15;
            this.reg_but.Text = "Зареєструватись";
            this.reg_but.UseVisualStyleBackColor = true;
            // 
            // reg_back
            // 
            this.reg_back.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.reg_back.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.reg_back.Font = new System.Drawing.Font("Comic Sans MS", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.reg_back.Location = new System.Drawing.Point(391, 434);
            this.reg_back.Name = "reg_back";
            this.reg_back.Size = new System.Drawing.Size(172, 44);
            this.reg_back.TabIndex = 16;
            this.reg_back.Text = "Назад";
            this.reg_back.UseVisualStyleBackColor = true;
            // 
            // label_repeat_pass
            // 
            this.label_repeat_pass.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label_repeat_pass.BackColor = System.Drawing.Color.Transparent;
            this.label_repeat_pass.Cursor = System.Windows.Forms.Cursors.Default;
            this.label_repeat_pass.Font = new System.Drawing.Font("Comic Sans MS", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_repeat_pass.ForeColor = System.Drawing.Color.DarkOrange;
            this.label_repeat_pass.Location = new System.Drawing.Point(154, 264);
            this.label_repeat_pass.Margin = new System.Windows.Forms.Padding(0);
            this.label_repeat_pass.Name = "label_repeat_pass";
            this.label_repeat_pass.Size = new System.Drawing.Size(296, 63);
            this.label_repeat_pass.TabIndex = 13;
            this.label_repeat_pass.Text = "Повторіть пароль";
            this.label_repeat_pass.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_pass_reg
            // 
            this.label_pass_reg.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label_pass_reg.BackColor = System.Drawing.Color.Transparent;
            this.label_pass_reg.Cursor = System.Windows.Forms.Cursors.Default;
            this.label_pass_reg.Font = new System.Drawing.Font("Comic Sans MS", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_pass_reg.ForeColor = System.Drawing.Color.DarkOrange;
            this.label_pass_reg.Location = new System.Drawing.Point(154, 192);
            this.label_pass_reg.Margin = new System.Windows.Forms.Padding(0);
            this.label_pass_reg.Name = "label_pass_reg";
            this.label_pass_reg.Size = new System.Drawing.Size(157, 57);
            this.label_pass_reg.TabIndex = 10;
            this.label_pass_reg.Text = "Пароль";
            this.label_pass_reg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // log_reg
            // 
            this.log_reg.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.log_reg.BackColor = System.Drawing.Color.Transparent;
            this.log_reg.Cursor = System.Windows.Forms.Cursors.Default;
            this.log_reg.Font = new System.Drawing.Font("Comic Sans MS", 16.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.log_reg.ForeColor = System.Drawing.Color.DarkOrange;
            this.log_reg.Location = new System.Drawing.Point(102, 120);
            this.log_reg.Margin = new System.Windows.Forms.Padding(0);
            this.log_reg.Name = "log_reg";
            this.log_reg.Size = new System.Drawing.Size(155, 39);
            this.log_reg.TabIndex = 9;
            this.log_reg.Text = "Логін";
            this.log_reg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 196F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(385, 1);
            this.tableLayoutPanel1.TabIndex = 17;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(276, 371);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.92344F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 76.07655F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(417, 63);
            this.tableLayoutPanel2.TabIndex = 18;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel3.Controls.Add(this.reg_but, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(411, 56);
            this.tableLayoutPanel3.TabIndex = 18;
            // 
            // reg_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.BackgroundImage = global::lab_ipz.Properties.Resources.reg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(973, 531);
            this.Controls.Add(this.textBox_reg_rep_pass);
            this.Controls.Add(this.reg_back);
            this.Controls.Add(this.textBox_reg_pass);
            this.Controls.Add(this.label_repeat_pass);
            this.Controls.Add(this.textBox_reg_log);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.label_pass_reg);
            this.Controls.Add(this.log_reg);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(852, 480);
            this.Name = "reg_form";
            this.Text = "Реєстрація";
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_reg_pass;
        private System.Windows.Forms.TextBox textBox_reg_log;
        private System.Windows.Forms.TextBox textBox_reg_rep_pass;
        private System.Windows.Forms.Button reg_but;
        private System.Windows.Forms.Button reg_back;
        private System.Windows.Forms.Label label_repeat_pass;
        private System.Windows.Forms.Label label_pass_reg;
        private System.Windows.Forms.Label log_reg;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
    }
}