﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace ipz_lab_server
{
    public class ClientObject
    {
        public TcpClient client;
        public ClientObject(TcpClient tcpClient)
        {
            client = tcpClient;
        }
        public string get_message(NetworkStream str)
        {
            byte[] data = new byte[64];// буфер для отриманих даних
            string mes;
            NetworkStream stream = str;
            StringBuilder builder = new StringBuilder();
            int bytes = 0;
            do
            {
                bytes = stream.Read(data, 0, data.Length);
                builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
            }
            while (stream.DataAvailable);

            mes = builder.ToString();
            return mes;
        }

        public void send_message(NetworkStream str, string message)
        {
            byte[] data = new byte[64];// буфер для отриманих даних
            NetworkStream stream = str;
            data = Encoding.UTF8.GetBytes(message);
            stream.Write(data, 0, data.Length);
        }

        public void Process()
        {
            NetworkStream stream = null;
            try
            {
                string sep = "$";
                string log = "@";
                string reg = "#";
                string my_q = "*";
                string leave_q = "%";
                stream = client.GetStream();
                while (true)
                {
                    // отримуємо повідомлення
                    string message = get_message(stream);

                    if (message == "exit" || message == log + "exit" || message == reg + "exit" || message == my_q + "exit"
                        || message == leave_q + "exit" || message == message[0].ToString() + "exit")
                    {
                        break;
                    }
                    if (message[0].ToString() == log)
                    {
                            string login = message.Substring(message.IndexOf(log) + 1, message.IndexOf(sep)-1);
                            string password = message.Substring(message.IndexOf(sep) + 1);
                            Console.WriteLine("Отримано логін: '" + login + "' та пароль: '" + password+ "'");
                            sql_server sql = new sql_server();
                            if (sql.get_sql(login, password,"registration") == true)
                            {
                                // відправляємо повідомлення назад
                                send_message(stream, message + "yes");
                                Console.WriteLine("Користувача знайдено у базі даних,  вхід дозволено");
                            }
                            else
                            {
                                send_message(stream,message);
                                Console.WriteLine("Користувача не знайдено у базі даних,  вхід заборонено");
                            }
                    }
                    else if (message[0].ToString() == reg)
                    {
                        sql_server sql = new sql_server();
                        if (message.Contains(sep))
                        {
                            string login = message.Substring(message.IndexOf(reg) + 1, message.IndexOf(sep) - 1);
                            string password = message.Substring(message.IndexOf(sep) + 1);
                            Console.WriteLine("Зареєстровано користувача з логіном: '" + login + "' та паролем: '" + password + "'");
                            sql.Connect_reg_sql(login, password);
                            // відправляємо повідомлення назад
                            send_message(stream, message);
                        }
                        else
                        {
                            string login = message.Substring(message.IndexOf(reg) + 1);
                            if (sql.get_sql(login, "", "checking") == true)
                            {
                                Console.WriteLine("Логін " + login + " існує у базі даних");
                                // відправляємо повідомлення назад
                                send_message(stream, message + "yes");
                            }
                            else
                            {
                                send_message(stream, message);
                            }
                        }
                    }
                    else if (message[0].ToString() == my_q)
                    {
                        string login = message.Substring(message.IndexOf(my_q) + 1);
                        sql_server sql = new sql_server();
                        if (sql.get_sql(login, "", "queue") == true)
                        {
                            // відправляємо повідомлення назад, що такий клієнт уже в черзі
                            send_message(stream, message + "yes");
                        }
                        else
                        {
                            Console.WriteLine("Користувач " + login + " вступив в чергу");
                            //записуємо клієнта в чергу
                            sql.insert_q(login);
                            send_message(stream, message);
                        }
                    }
                    else if (message[0].ToString() == leave_q)
                    {
                        string login = message.Substring(message.IndexOf(leave_q) + 1);
                        sql_server sql = new sql_server();
                        sql.delete_q(login);
                        Console.WriteLine("Користувач " + login + " вийшов з черги");
                        // відправляємо повідомлення назад
                        send_message(stream, message);
                    }
                    else if(message.Contains("$check"))
                    {
                        string login = message.Substring(0,message.IndexOf(sep));
                        sql_server sql = new sql_server();
                        if (sql.get_sql(login, "", "queue") == true)
                        {
                            // відправляємо повідомлення назад, що такий клієнт уже в черзі
                            send_message(stream, message + "yes");
                        }
                        else
                        {
                            send_message(stream, message);
                        }
                    }
                    else if(message.Contains("actual_id"))
                    {
                        string login = message.Substring(0, message.IndexOf("actual_id"));
                        sql_server sql = new sql_server();
                        // відправляємо повідомлення назад
                        send_message(stream, sql.get_id_q(login));
                    }
                    else if (message.Contains("all_id"))
                    {
                        string login = message.Substring(0, message.IndexOf("all_id"));
                        sql_server sql = new sql_server();
                        // відправляємо повідомлення назад
                        send_message(stream, sql.get_all_q(login));
                    }
                    else if (message.Contains("delete_last"))
                    {
                        string login = message.Substring(0, message.IndexOf("delete_last"));
                        sql_server sql = new sql_server();
                        sql.delete_last();
                        Console.WriteLine("Користувача " + login + " видалено з черги");
                        // відправляємо повідомлення назад
                        send_message(stream, message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
               // if (stream != null)
                    stream.Close();
               // if (client != null)
                    client.Close();
               // client.Dispose();
            }
        }
    }
}
