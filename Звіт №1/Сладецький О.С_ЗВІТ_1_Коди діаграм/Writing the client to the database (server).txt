@startuml
start
:Receiving a request to create an account<
:Request processing within a certain time;
:Add customer account to database;
if(Еhe addition was successful?) then (Yes)
:Send information to the client part about the success of the add-on>
else(No)
:Send error information>
endif
stop
@enduml