@startuml
start
note right: Database
:Receiving a request to delete data<
:Deleting data;
stop
@enduml