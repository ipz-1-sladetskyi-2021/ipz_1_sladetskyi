@startuml
start
:Getting a request about the number of people in the queue<
:Request processing within a certain time;
:Get information about the number of users in the queue from the database;
:Send information about the number of users in the queue>
stop
@enduml