@startuml
start
:Receiving a user request<
:Checking the correctness of the request;
if (The request is correct?) then (yes)
:Continuing the server operation;
else(no)
:Sending a message about an incorrect request>
endif
stop
@enduml