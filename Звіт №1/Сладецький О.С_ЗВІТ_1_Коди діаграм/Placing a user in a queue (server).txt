@startuml
start
:Receiving a request to place a client in the queue<
:Request processing within a certain time;
:Get information about the number of users in the queue from the database<
:Placing the client in a queue in the database;
if(Placement was successful?) then (Yes)
:Send information to the client part about the success of the add-on>
else(No)
:Send error information>
endif
stop
@enduml