@startuml
start
:Registration in the system;
:Entering your username<
repeat:Entering a password;
:Re-entering your password<
backward:Passwords don't match;
note right:Error
repeat while (Passwords match?) is (No) not (Yes)
:Sending data to the server>
:Waiting for a response from the server for a certain amount of time;
if(The server responds?) then (No)
:Displaying the message: server is not responding;
:Return to the registration screen;
else (Yes)
:Response analysis;
if(Got an error?) then (Yes)
:Displaying an error message;
:Return to the registration screen;
else(No)
if(Is your username busy?) then (Yes)
:Displaying a message: This username is busy;
:Return to the registration screen;
else(No)
:Displaying a message: you have successfully registered;
endif
endif
endif
stop
@enduml